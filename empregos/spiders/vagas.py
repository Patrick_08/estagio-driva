import scrapy
from scrapy.crawler import CrawlerProcess
import csv


class EmpregosGupy(scrapy.Spider):
    name = 'empregos'
    start_urls = ['https://portal.gupy.io/job-search/term=vagas']

    def parse(self, response):
        csv_filename = "vagas.csv"

        # Abrir o arquivo CSV para escrita e criar o objeto DictWriter
        with open(csv_filename, 'w', newline='', encoding='utf-8') as csvfile:
            fieldnames = ['Empresa', 'Vaga', 'Local', 'Modelo', 'Tipo', 'PcD', 'Data de Publicação']
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

            writer.writeheader()

            processed_items = set()  # Criar um conjunto para controlar itens processados
            first_iteration = True
            # Iterar sobre os elementos e escrever cada item no CSV
            for empregos in response.css('div'):
                if empregos.css('h2::text').get() is not None:
                    if first_iteration == True:
                        first_iteration = False
                        continue
                    item = {
                        'Empresa': empregos.css('p::text').get(),
                        'Vaga': empregos.css('h2::text').get(),
                        'Local': empregos.css('span[data-testid="job-location"]::text').get(),
                        'Modelo': empregos.css('.hCmVmU:nth-child(2) .cezNaf::text').get(),
                        'Tipo': empregos.css('.hCmVmU:nth-child(3) .cezNaf::text').get(),
                        'PcD': empregos.css('.hCmVmU:nth-child(4) .cezNaf::text').get(),
                        'Data de Publicação': empregos.css('.inqtnx::text').get().split(': ')[1]
                    }

                    # Verificar se o item já foi processado
                    item_tuple = tuple(item.items())
                    if item_tuple in processed_items:
                        continue  # Item já processado, pular para o próximo
                    processed_items.add(item_tuple)

                    # Escrever item no CSV
                    writer.writerow(item)

                    # Yield para permitir que o Scrapy processe a saída
                    yield item
process = CrawlerProcess()
process.crawl(EmpregosGupy)
process.start()