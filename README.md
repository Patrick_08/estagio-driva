ESTAGIO DRIVA

Projeto de Web Scraping de vagas de empregos do site portal.gupy.io

Este projeto se trata de um Web Scraping criado para extrair informações de empregos de uma página de busca de empregos.

Funcionamento Geral

1 - A spider começa na URL de partida: https://portal.gupy.io/job-search/term=vagas.

2 - A spider utiliza seletores CSS para identificar e extrair informações de empregos da página, incluindo empresa, vaga, local, modelo, tipo, PcD (Pessoa com Deficiência) e data de publicação.

3 - As informações extraídas são organizadas em um dicionário e verificadas quanto a duplicatas para evitar a inclusão de entradas repetidas.

4 - Os dados são escritos em um arquivo CSV chamado vagas.csv, com cabeçalhos apropriados.

Lógica do Projeto

1 - Importando Módulos:

import scrapy: Isso importa o módulo Scrapy, que é um framework de scraping amplamente utilizado.
from scrapy.crawler import CrawlerProcess: Isso importa a classe CrawlerProcess do Scrapy, que é usada para controlar e executar as spiders.

2 - Definindo a Classe EmpregosGupy:

class EmpregosGupy(scrapy.Spider): Cria uma classe chamada EmpregosGupy que herda da classe scrapy.Spider.

3 - Configurando a Spider:

name = 'empregos': Define um nome para a spider, que pode ser usado para identificá-la.
start_urls = ['https://portal.gupy.io/job-search/term=vagas']: Especifica a URL de partida para a spider.

4 - Definindo o Método parse:

Este é o método principal que contém a lógica do programa. Ele é chamado para cada resposta de solicitação HTTP feita pela spider.
Ele começa abrindo um arquivo CSV chamado "vagas.csv" para escrita.
Define os cabeçalhos do CSV usando fieldnames.
Cria um objeto DictWriter que será usado para escrever os dados no arquivo CSV.

5 - Iterando sobre os Elementos:

A spider começa a iterar sobre os elementos div na resposta da solicitação HTTP.
Verifica se o texto dentro da tag h2 não é "None", ou seja, se existe texto dentro dessa tag. Isso é feito para garantir que apenas as seções relevantes sejam processadas.
6 - Extraindo Dados:

Para cada elemento div, os detalhes do emprego são extraídos usando seletores CSS. Os detalhes incluem empresa, vaga, local, modelo, tipo, PcD (Pessoa com Deficiência) e data de publicação.
Os detalhes são armazenados em um dicionário chamado item.

7 - Evitando Duplicatas:

Antes de escrever o item no CSV, o código verifica se o item já foi processado, comparando as tuplas dos itens já processados.
Se o item já tiver sido processado, ele é ignorado e a iteração continua para o próximo elemento.

8 - Escrevendo no Arquivo CSV:

Se o item não for uma duplicata, ele é escrito no arquivo CSV usando o objeto DictWriter.

9 - Usando o Scrapy para Executar a Spider:

Cria uma instância da classe CrawlerProcess que gerenciará a execução da spider.
Inicia a spider EmpregosGupy usando o método crawl.
Inicia o processo de scraping usando o método start.

ANÁLISE DOS DADOS

Em minha análise pude perceber que as listagens de vagas são atualizadas diariamente, demonstrando um compromisso forte com a precisão das informações fornecidas aos candidatos. Entre as oportunidades disponíveis, destacam-se posições em empresas renomadas de grande prestígio no mercado de trabalho, incluindo multinacionais como O Boticário e Honda. Essa representatividade no cenário corporativo confere ainda mais valor às vagas oferecidas.

Observamos uma distribuição ampla de vagas por todo o Brasil, com uma concentração particular na região Sudeste. Enquanto a região Norte possui uma presença um pouco menos proeminente, a disponibilidade de oportunidades em diversas localidades é notável.

Uma característica altamente positiva é a inclusão de vagas que acolhem pessoas com deficiência (PCDs), juntamente com a oferta de oportunidades exclusivas para mulheres e indivíduos negros. Esse compromisso com a diversidade e a igualdade é uma marca importante das vagas analisadas.

Outro aspecto relevante é que algumas vagas permite o trabalho remoto, refletindo a crescente tendência de modelos flexíveis de emprego. Essa flexibilidade é uma vantagem para profissionais que buscam equilibrar suas responsabilidades profissionais e pessoais.

No que diz respeito aos requisitos, a análise revelou que a maioria das vagas não exige uma extensa experiência anterior, sendo frequentemente voltada para posições de assistente. No entanto, também foram identificadas oportunidades para cargos de maior responsabilidade e hierarquia.

No geral, a diversidade e abrangência das vagas oferecem uma ampla variedade de opções para profissionais de diferentes níveis de experiência e habilidades, contribuindo para um panorama dinâmico e acessível no mercado de trabalho.